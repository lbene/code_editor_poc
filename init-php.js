createPhpInterpreter()
  .then((php) => ({
    php,
    output: new Output(php),
  }))
  .then((editorContext) => {
    window.editorContext = editorContext;
  });

/**
 * Creates a PHP Interpreter that is ready to run code.
 *
 * @returns {Promise<EventTarget>}
 */
async function createPhpInterpreter() {
  // We are wrapping a new promise over the import, because the PHPWeb object needs to actually
  // initialize after the import has succeede. When it is ready, it will throw the 'ready' event and
  // we are resolving this promise only on that event.
  return import("https://cdn.jsdelivr.net/npm/php-wasm/PhpWeb.mjs")
    .then(({ PhpWeb }) => {
      const php = new PhpWeb();

      return new Promise((resolve) => {
        php.addEventListener("ready", () => {
          resolve(php);
        });
      });
    })
    .catch((err) => {
      console.error("Create Interpeter: Could not import WASM module");
      console.error(err);
    });
}

/**
 * Wraps the stdout and stderror streams, so that we can collect
 * the results a bit more easily.
 */
class Output {
  /**
   * @type {EventTarget}
   */
  php;

  stdout = "";

  /**
   * @param {EventTarget} php
   */
  constructor(php) {
    php.addEventListener("output", (event) => {
      this.stdout += event.detail[0];
    });

    // We are not processing php://stderr for now
    // because errors are streamed to php://stdout
    php.addEventListener("error", (event) => {
      console.error(event.detail);
    });

    this.php = php;
  }

  collect() {
    const result = this.stdout;
    this.stdout = "";

    return result;
  }
}
